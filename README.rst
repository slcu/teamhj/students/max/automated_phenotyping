=====================
automated_phenotyping
=====================

*automated_phenotyping* is a python class and collection of python functions for
the automated extraction of features of a shoot apical meristem (SAM) from a 
stack of confocal microscope images.
Its main features are:

* Extraction of the SAMs surface using active contour without edges [1]_ (ACWE).
* Separation of the SAMs features into primordiae and the meristem.
* Evaluation of the features in terms of location, size and divergence angle.

Dependencies
============

This software is written for Python 2.7.6

Used packages are:

* SciPy 0.19.0
* NumPy 1.13.0
* VTK 5.8.0
* pandas 0.17.1
* tissueviewer 

Installation
============
Download *automated_phenotyping.py* for example by cloning the repository.
Add *automated_phenotyping.py* to your pythonpath or put it in the same folder
as your script and import it.

.. code:: python

  import automated_phenotyping as ap

A short example
===============
This is a short example of how to use *automated_phenotyping*. Say, the image
has the name ''image.tif''. The following code fits a contour onto the image.

.. figure:: images/raw_image.png
   :height: 256
   :width: 256
   
   3D view of image.tif

.. code:: python

  import automated_phenotyping as ap
  
  image, _ = tiffread('image.tif')  # import the image as numpy array
  A = ap.AutoPhenotype(data = image)  # initialise object
  A.set_contour_to_box()  # initialise contour for fit
  A.contour_fit(iterations = 100, weighting = 1., iterate_smooth = 1)  # fit

After this code is run, both the image data and the contour are stored in the
object A.

.. figure:: images/ACWE_animation-crop.gif
   :scale: 50 %
   
   ACWE fit onto a SAM. Visualisation of the code above.

In the next step, the contour is converted into a mesh, which is then smoothed
and sliced along its curvature to prepare for the extraction of the SAM
features.

.. code:: python
  
  A.mesh_conversion()  # create a mesh of the contours surface
  A.smooth_mesh(iterations = 200, relaxation_factor = 0.2)  # smoothe the mesh
  A.curvature_slice(threshold = 0., curvature_type = 'mean')  # slice the mesh

The mesh data is also stored in A.

Now that the mesh is sliced, the next step is to extract the SAMs features and
start the evaluation by fitting spheres onto the extracted features.

.. code:: python

  A.feature_extraction(res = 100)  # extract the features of the meristem
  A.sphere_fit()  # fit spheres onto the extracted features
  A.sphere_evaluation()  # calculate the results of the sphere fit

The features and results are stored in A.

The results can be accessed by using

.. code:: python

  print A.results  # display all results
  print A.get_div_angle(condition = 'sphere_R')  # display the divergence angle
                                                 # sorted by condition
  A.show_spheres()  # displays the fitted spheres in a 3D viewer

The object A can be saved and loaded at any point of the decribed process by 
using

.. code:: python

  A.save('/path/foldername')
  A.load('/path/foldername')


References
==========

.. [1] *A morphological approach to curvature-based evolution
   of curves and surfaces*. Pablo Márquez-Neila, Luis Baumela, Luis Álvarez.
   In IEEE Transactions on Pattern Analysis and Machine Intelligence (PAMI). 
   2014
